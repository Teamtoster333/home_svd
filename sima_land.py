from baseapp import BasePage
from selenium.webdriver.common.by import By

class SimaLocators:
    Locator_login = (By.ID, "login-entity")
    Locator_password = (By.ID, "login-password")
    Locator_auth_button = (By.XPATH, "//*[@id='page-header']/div/div[1]/a")
    Locator_singin_button = (By.XPATH, "//*[@id='login-form']/div[2]/div[5]/input")
    Locator_profile_button = (By.XPATH, "//*[@id='cabinet-page']/div/div[3]/div/div/div/a[6]")
    Locator_logout_button = (By.ID, "logout")
    Locator_menu_button = (By.XPATH, "//*[@id='page-header']/div/div[1]/button")
    Locator_catalog_link = (By.XPATH, "//*[@id='main-categories-wrapper']/div[1]/div/a")
    Locator_furniture_link = (By.XPATH, "//*[@class='category-block layout-inner'][1]/div/div/div[1]/a")
    Locator_furniture2_link = (By.XPATH, "//*[@class='item-with-image'][1]/a")
    Locator_product_link = (By.XPATH, "//*[@id='category-page']/div/div/div[2]/div[1]/div[1]/div/div[1]/div/div[1]/a/img")
    Locator_add_product_button = (By.XPATH, "//*[@id='product']/div/div/div[15]/div/button")
    Locator_basket_button = (By.XPATH, "//*[@id='page-header']/div/nav/a[2]")
    Locator_check_add_product = (By.XPATH, "//*[@class='cart-info with-layout']")
    Locator_delete_dialog_popup = (By.XPATH, "//*[@class='dialog-cart-button js-opener-dialog-popup']")
    Locator_delete_button = (By.XPATH, "/html/body/div[1]/div[3]/div[4]/span[1]")
    Locator_emty_basket = (By.XPATH, "//*[@class='empty-cart with-layout']")

class LoginHelper(BasePage):
    def enter_word_login(self, word):
        login_field = self.find_element(SimaLocators.Locator_login)
        login_field.click()
        login_field.send_keys(word)
        return login_field

    def enter_word_password(self, word):
        password_field = self.find_element(SimaLocators.Locator_password)
        password_field.click()
        password_field.send_keys(word)
        return password_field

    def click_auth_button(self):
        return self.find_element(SimaLocators.Locator_auth_button,time=5).click()

    def click_singin_button(self):
        return self.find_element(SimaLocators.Locator_singin_button,time=5).click()

    def click_profile_button(self):
        return self.find_element(SimaLocators.Locator_profile_button,time=10).click()

    def click_logout_button(self):
        return self.find_element(SimaLocators.Locator_logout_button,time=10).click()

    def click_menu_button(self):
        return self.find_element(SimaLocators.Locator_menu_button,time=10).click()

    def click_catalog_link(self):
        return self.find_element(SimaLocators.Locator_catalog_link,time=10).click()

    def click_furniture_link(self):
        return self.find_element(SimaLocators.Locator_furniture_link,time=10).click()

    def click_furniture2_link(self):
        return self.find_element(SimaLocators.Locator_furniture2_link,time=10).click()

    def click_product_link(self):
        return self.find_element(SimaLocators.Locator_product_link,time=10).click()

    def click_add_product_button(self):
        return self.find_element(SimaLocators.Locator_add_product_button,time=10).click()

    def click_basket_button(self):
        return self.find_element(SimaLocators.Locator_basket_button,time=10).click()

    def check_add_product(self):
        return self.find_element(SimaLocators.Locator_check_add_product,time=10)

    def click_delete_dialog_popup(self):
        return self.find_element(SimaLocators.Locator_delete_dialog_popup,time=10).click()

    def click_delete_button(self):
        return self.find_element(SimaLocators.Locator_delete_button,time=10).click()

    def check_emty_basket(self):
        return self.find_element(SimaLocators.Locator_emty_basket,time=10)
