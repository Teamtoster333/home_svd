import time
from selenium import webdriver
from sima_land import LoginHelper


def test_sima_land(browser):
	sima_main_page = LoginHelper(browser)
	sima_main_page.go_to_site()
	sima_main_page.click_auth_button()
	sima_main_page.enter_word_login("teamtoster@gmail.com")
	sima_main_page.enter_word_password("321321")
	sima_main_page.click_singin_button()
	sima_main_page.click_catalog_link()
	sima_main_page.click_furniture_link()
	sima_main_page.click_furniture2_link()
	sima_main_page.click_product_link()
	sima_main_page.click_add_product_button()
	sima_main_page.click_basket_button()
	sima_main_page.check_add_product()
	sima_main_page.click_auth_button()
	sima_main_page.click_profile_button()
	sima_main_page.click_logout_button()
	time.sleep (60)
	sima_main_page.click_auth_button()
	sima_main_page.enter_word_login("teamtoster@gmail.com")
	sima_main_page.enter_word_password("321321")
	sima_main_page.click_singin_button()
	sima_main_page.click_basket_button()
	sima_main_page.check_add_product()